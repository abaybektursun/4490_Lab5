package lab5out;

import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

public class LoginData implements Serializable{
    String pass;
    String uname;
    public LoginData(String uname, String pass){
        this.pass  = pass;
        this.uname = uname;
    }
    
    // Getters and setters
    public void setPass(String pass)
    {
        this.pass = pass;
    }
    public void setUname(String uname)
    {
        this.uname = uname;
    }
    public String getPass()
    {
        return this.pass;
    }
    public String getUname()
    {
        return this.uname;
    }
}