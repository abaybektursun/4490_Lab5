package lab5out;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.*;

public class LoginPanel extends JPanel
{
  private JTextField username;
  private JPasswordField password;

  public JTextField getUserName()
  {
    return username;
  }
  
  public JPasswordField getPassword()
  {
    return password;
  }
  
  public LoginPanel(CardLayout cl, JPanel container, ChatClient client)
  {    

    JPanel inner = new JPanel(new GridLayout(5,1));
    JLabel jlabel = new JLabel("Login",JLabel.CENTER);
    username = new JTextField(15);
    password = new JPasswordField(15);
    JButton submit = new JButton("Submit");
    submit.setPreferredSize(new Dimension(30,30));
    //
    JButton cancel = new JButton("Cancel");
    cancel.setPreferredSize(new Dimension(30,30));
    //
    
    LoginControl lc = new LoginControl(cl,container,client);
    submit.addActionListener(lc);
    //
    cancel.addActionListener(lc);
    //
    
    
    inner.add(jlabel);
    inner.add(username);
    inner.add(password);
    inner.add(submit);
    //
    inner.add(cancel);
    //
    
    this.add(inner);
    
    
  }
  
 
}
