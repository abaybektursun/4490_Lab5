package lab5out;

import java.awt.*;
import javax.swing.*;
import java.io.IOException;

public class CreateAccount extends JPanel{
    private JTextField     uname;
    private JPasswordField pass;
    private JPasswordField pass_re;
    
    public CreateAccount(CardLayout cl, JPanel container, ChatClient client){
        JPanel inner   = new JPanel(new GridLayout(6,1));
        JLabel jlabel  = new JLabel("Enter the Username and Password",JLabel.CENTER);
        username       = new JTextField(15);
        password       = new JPasswordField(15);
        JButton submit = new JButton("Submit");
        JButton cancel = new JButton("Cancel");
        
        submit.setPreferredSize(new Dimension(30,30));
        cancel.setPreferredSize(new Dimension(30,30));
        
        CreateAccountControl cac = new CreateAccountControl(cl,container,client);
        submit.addActionListener(cac);
        cancel.addActionListener(cac);
        
        inner.add(jlabel);
        inner.add(username);
        inner.add(password);
        inner.add(submit);
        inner.add(cancel);
        
        this.add(inner);
    }
    
    public JTextField getUname()
    {
        return this.uname;
    }
    
    public JPasswordField getPass()
    {
        return this.pass;
    }
}