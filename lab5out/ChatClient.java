package lab5out;

import ocsf.client.AbstractClient;
//import ocsf.server.ConnectionToClient;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.io.IOException;
import javax.swing.UIManager;

public class ChatClient extends AbstractClient{
    
    private JLabel status;
    private JTextArea serverMsg;
    private JTextField clientID;
    
    private CardLayout cl;
    private JPanel container;

    
    public ChatClient()
    {
        super("localhost",8300);
    }
    
    public void handleMessageFromServer(Object arg0)
    {
        try
        {
            
            //int errCode = (Integer)arg0;
            String errCode = (String)arg0;
            System.out.println("worked! client");
            switch (errCode)
            {
                
                case "0":
                    cl.show(container, "3");
                    break;
                //case 1:
                //    break;
                //case 2:
                //    break;
                //case 3:
                //    break;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        if (Integer.class.isInstance(arg0))
        {
            
            
        }
        if (arg0 instanceof String)
        {
            serverMsg.append("Server: " + (String)arg0 + "\n");
            
            // If it string contains only one colon, assume key value pair                      //
            int colons = 0;                                                                     //
            int colonIdx = -1;                                                                  //
            String inKey = new String();                                                        //
            String inVal = new String();                                                        //
            for(int idx = 0; idx < ((String)arg0).length(); idx++)                              //
            {                                                                                   //
                if(((String)arg0).charAt(idx) == ':')                                           //
                {                                                                               //
                    colons++;                                                                   //
                    colonIdx = idx;                                                             //
                }                                                                               //
            }                                                                                   //
            if (colons == 1)                                                                    //
            {                                                                                   //
                inKey = ((String)arg0).substring(0, colonIdx).trim();                           //
                inVal = ((String)arg0).substring(colonIdx+1, ((String)arg0).length()).trim();   //
            }                                                                                   //
            if(inKey.equals("username"))                                                        //
            {                                                                                   //
                clientID.setText(inVal);                                                        //
            }                                                                                   //
            //----------------------------------------------------------------------------------//
        }
        else
        {
            System.out.println("arg0 is not string!\n");
        }
    }
    
    public void connectionException (Throwable exception) 
    {
        // Add stuff here
    }
    public void connectionEstablished() 
    {
       //this.status.setForeground(Color.green);
       //this.status.setText("Connected");
    }
    public void setStatus(JLabel status)
    {
        this.status = status;
    }
    public void setServerMsg(JTextArea serverMsg)
    {
        this.serverMsg = serverMsg;
    }
    public void setClientID(JTextField clientID)
    {
        this.clientID = clientID;
    }
    public void connectionClosed()
    {
        //this.status.setForeground(Color.red);
        //this.status.setText("Not Connected");
    }
    public void setLayout(CardLayout cl)
    {
        this.cl = cl;
    }
    public void setContainer(JPanel container)
    {
        this.container = container;
    }
    
}