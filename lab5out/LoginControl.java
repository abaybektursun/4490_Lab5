package lab5out;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

public class LoginControl implements ActionListener
{
    private ChatClient client;
    private CardLayout cl;
    private JPanel container;
    private String uname;
    private String pass;
  
  public LoginControl(CardLayout cl, JPanel container,ChatClient client)
  {
    this.cl        = cl;
    this.container = container;
    this.client    = client;
  }
  
  public void actionPerformed(ActionEvent ae)
  {
    //cl.show(container, "2");
    
    int count     = container.getComponentCount();
    LoginPanel lp = (LoginPanel)container.getComponent(1);
    
    //
    String command = ae.getActionCommand();
    if (command.equals("Cancel"))
    {
      cl.show(container, "1");
    }
    //
    
    uname = lp.getUserName().getText();
    pass  = String.valueOf(lp.getPassword().getPassword());
    
    if (command.equals("Submit"))
    {
        try{
            client.sendToServer( new LoginData(uname, pass) ); 
        }
        catch (IOException ioex)
        {
        }
    }
    
  }
}
