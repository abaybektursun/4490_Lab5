package lab5out;

import ocsf.server.AbstractServer;
import ocsf.server.ConnectionToClient;
import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.*;


public class ChatServer extends AbstractServer
{
  private JTextArea log;
  private JLabel status;
  ArrayList<LoginData> users;
  String fname;
  
  public ChatServer()
  {
    super(12345);
    
    fname = "lab5out/DatabaseFile";    
    users = new ArrayList<LoginData>();
    readDataTemp(fname);
    for (LoginData a_user : users)
    {
        System.out.println(a_user.getUname() + a_user.getPass());
    }
  }
  
  public ChatServer(int port)
  {
    super(port);
  }
  
  public void setLog(JTextArea log)
  {
    this.log = log;
  }
  
  public JTextArea getLog()
  {
    return log;
  }
  
  public void setStatus(JLabel status)
  {
    this.status = status;
  }
  
  public JLabel getStatus()
  {
    return status;
  }
  
  
  
  @Override
  protected void handleMessageFromClient(Object arg0, ConnectionToClient client)
  {
        if (arg0 instanceof LoginData){
            if (((LoginData)arg0).getUname().charAt(0) == '$')
            {
                LoginData newUser = ((LoginData)arg0);
                writeDataTemp(fname,newUser);
                users.add(newUser);
            }
            else{
                try 
                {
                    System.out.println("Message from Client" + ((LoginData)arg0).getPass() + " : " + ((LoginData)arg0).getUname());
                    LoginData connected_user = findUser( ((LoginData)arg0).getUname() );
                    if (connected_user != null )
                    {
                        if( connected_user.getPass().equals( ((LoginData)arg0).getPass() ) )
                        {
                            String z = "0";
                            client.sendToClient(z);
                            System.out.println("Worked!");
                        }
                    }
                        
                } 
                catch (Exception ex) 
                {ex.printStackTrace();}
            }
        }

        // TODO Auto-generated method stub
        //System.out.println("Message from Client" + arg0.toString() + arg1.toString());
        //log.append("Message from Client" + arg0.toString() + arg1.toString() + "\n");

  }
  
  	
	public void listeningException(Throwable exception) {
		this.log.append(exception.toString() + "\n");
        this.status.setForeground(Color.red);
        this.status.setText("Exception Occurred when Listening");
        
        //Display info about the exception
        System.out.println("Listening Exception:" + exception);
        exception.printStackTrace();
        System.out.println(exception.getMessage());
        /*if (this.isListening())
        {
            log.append("Server not Listening\n");
            status.setText("Not Connected");
            status.setForeground(Color.RED);
        }*/
	}
  
  public void serverStarted() {
		this.log.append("Server Started\n");
        this.status.setForeground(Color.green);
        this.status.setText("Listening");
	}
    
    public void serverStopped()
    {
        this.log.append("Server Stopped Accepting New Clients - Press Listen to Start Accepting New Clients\n");
        this.status.setForeground(Color.red);
        this.status.setText("Stopped");
    }
  
    public void serverClosed()
    {
        this.log.append("Server and all current clients are closed - Press Listen to Restart\n");
        this.status.setForeground(Color.red);
        this.status.setText("Close");
    }

    public void clientConnected(ConnectionToClient client)
    {
         try{
            client.sendToClient("username:Client-" + client.getId());
        }   
        catch (IOException send_exc) {
            send_exc.printStackTrace();
        }
        
        this.log.append("Client " + client.getId() + " Connected" + "\n");
    }
  
  
    public void writeDataTemp(String fname, LoginData newUser)
    {
        String dataIn = newUser.getUname() + "\t" + newUser.getPass() + '\n';
        try{
    		String data = dataIn;
    		File file =new File(fname);
    		if(!file.exists()){
    			file.createNewFile();
    		}
    		FileWriter fileWritter = new FileWriter(file.getName(),true);
    	        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    	        bufferWritter.write(dataIn);
    	        bufferWritter.close();
    	}catch(IOException e){
    		e.printStackTrace();
    	}
    }
    
    public void readDataTemp(String fname)
    {
        String userData = null;
        try{
            BufferedReader br = new BufferedReader(new FileReader(fname));
        while ((userData = br.readLine()) != null) {
            // Split by whitespace
            String[] uname_pass = userData.split("\\s+");
            LoginData a_user = new LoginData(uname_pass[0], uname_pass[1]);
            users.add(a_user);
            
            
        }
        br.close();
        }
        catch (IOException ioex)
        {
            ioex.printStackTrace();
        }
        
        //try {
		//	File file = new File(fname);
		//	FileReader fileReader = new FileReader(file);
		//	BufferedReader bufferedReader = new BufferedReader(fileReader);
		//	StringBuffer stringBuffer = new StringBuffer();
		//	String userData;
		//	while ((userData = bufferedReader.readLine()) != null) {
        //
		//	}
		//	fileReader.close();
		//} catch (IOException e) {
		//	e.printStackTrace();
		//}
        
    }
    
    public LoginData findUser(String uname)
    {
        LoginData found = null;
        for (LoginData a_user :users) {
            if (a_user.getUname().equals(uname))
                found = a_user;
        }
        return found;
    }

}
